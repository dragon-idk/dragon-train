package rpj.railproject;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import rpj.railproject.items.RailProjItemsInit;

@Mod(RailProject.MOD_ID)
public class RailProject {
	public static final String MOD_ID = "railproject";
	public RailProject() {
	 	RailProjItemsInit.ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}
