package rpj.railproject.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import rpj.railproject.RailProject;

public class RailProjItemsInit {

 	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, RailProject.MOD_ID);

 	public static final RegistryObject<Item> SURVEY_MARKER = ITEMS.register("survey_marker", () -> new Item(new Item.Properties()));

}
